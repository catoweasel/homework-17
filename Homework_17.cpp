#include <iostream>
#include <cmath>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;

public:
    Vector()
    {}

    Vector (double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void show()
    {
        std::cout << "\n" << x << " " << y << " " << z;
    }

    double module()
    {

        return sqrt(x * x + y * y + z * z);
    }
};

int main()
{
    double xv = 0;
    double yv = 0;
    double zv = 0;

    std::cout << "Enter vector coordinates: " << "\n";
    std::cin >> xv >> yv >> zv;

    Vector v (xv, yv, zv);
    v.show();
    std::cout << "\n" << "Vector length = " << v.module();

    return 0;
}
